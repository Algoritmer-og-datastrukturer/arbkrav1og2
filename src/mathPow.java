import javax.swing.plaf.synth.SynthOptionPaneUI;

public class mathPow {


    //Oppgave 2.1-1
    public static double recursionExpV1(double x,int n){
        //base
        if(n==0){
            return 1;
        }
        else{
            return x * recursionExpV1(x,n-1);
            
        }
    }

    //Oppgave 2.2-3
    public static double recursionExpV2(double x ,int n){
        double k =0;
        if(n==0){
            return 1;
        }
        //for partall
        else if(n % 2 ==0){
            k = recursionExpV2(x,n/2);
            return k*k;
        }
        //for oddetall
        else{
            return x * recursionExpV2(x*x,(n-1)/2);

        }
    }


    public static void main(String[]args){

        int n = 100000; double x = 1;
        //  måle tid
        long startTime = System.nanoTime();

        for(int i =0; i <100000 ; i++){
            recursionExpV2(x,n);
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000; //divide by 1000000 to get milliseconds.
        System.out.println(duration+" duration");
//--------------------------------------------------------------------------------------
    }
}
