public  class aksjeAnalyse {
    /**
     *  for opprettelse av en aksje-annalyse
     *
     */
    int buyDay;
    int sellDay;
    int profit;

    public aksjeAnalyse(int buyDay, int sellDay, int profit) {
        this.buyDay = buyDay;
        this.sellDay = sellDay;
        this.profit = profit;
    }

    public int getBuyDay() {
        return buyDay;
    }

    public void setBuyDay(int buyDay) {
        this.buyDay = buyDay;
    }

    public int getSellDay() {
        return sellDay;
    }

    public void setSellDay(int sellDay) {
        this.sellDay = sellDay;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return
                "buyDay =" + buyDay +
                ", sellDay =" + sellDay +
                ", profit =" + profit ;
    }
}
