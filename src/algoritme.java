import java.util.Random;

public class algoritme {





    //lager ett nytt aray med prisforandringene justert perr dag
    public static int[] priceFluxToValue(int[] priceIncrease,int startValue){
      //gjøre om prisforandringen til verdi
        int nyVerdi = startValue;
        int [] nyttArray = new int[priceIncrease.length];
        for(int i=0; i < priceIncrease.length;i++){
            nyttArray[i] += nyVerdi;
            //adderer sammen start pluss endringen for dagen
            nyVerdi= nyttArray[i] + priceIncrease[i];
        }
        return nyttArray;
    }

    public static String analyse(int[]array,int value){
        //Tar inn prisendringene og startverdien, får et array med hvordan prisene ser ut etter dag
        int[] priceFluxArray = priceFluxToValue(array,value);

        int buyDay =0; int sellDay =0; int profit = 0;

    //sjekker alle dager en kan kjøpe aksjen på
        for(int buy =0; buy<priceFluxArray.length-1;buy++){

            // sjekk for alle dagen en kan selge på
            for(int sell = buy+1;sell < priceFluxArray.length;sell ++){

                if(priceFluxArray[buy] < priceFluxArray[sell]){

                        //sjekker profit per dag mot tidligere beste profit
                        if(priceFluxArray[sellDay] - priceFluxArray[buyDay] < priceFluxArray[sell]-priceFluxArray[buy]){
                        profit = priceFluxArray[sell]-priceFluxArray[buy];
                        buyDay = buy;
                        sellDay = sell;
                    }
                }
            }
        }
        return new aksjeAnalyse(buyDay,sellDay,profit).toString();
    }

    public static void main(String[]args){
        int[] priceIncrease = {-1,3,-9,2,2,-1,2,-1,-5};
        System.out.println(analyse(priceIncrease,1000));

        //fylle array med random tall-----------------------------------------
        int[]randomArray = new int[10];
        Random randNum = new Random();
        for (int i = 0;i<10;i++){
            randomArray[i] = randNum.nextInt();
        }

        //-----------------------------------------
        // funskjon for å måle tid
        long startTime = System.nanoTime();
        System.out.println(analyse(randomArray,10));
        long endTime = System.nanoTime();

        long duration = (endTime - startTime)/1000000;  //divide by 1000000 to get milliseconds.
        System.out.println(duration);
        //---------------------------------------------------------------------------------------
    }
}
