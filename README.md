# Arbkrav 1 & 2 


## Kompleksitetsanalyse , Arbkrav1
[aksjeAnalyse](https://gitlab.stud.idi.ntnu.no/Algoritmer-og-datastrukturer/arbkrav1og2/-/blob/main/src/aksjeAnalyse.java) og [algoritme](https://gitlab.stud.idi.ntnu.no/Algoritmer-og-datastrukturer/arbkrav1og2/-/blob/main/src/algoritme.java).

## Rekursjon , Arbkrav2
[mathPow](https://gitlab.stud.idi.ntnu.no/Algoritmer-og-datastrukturer/arbkrav1og2/-/blob/main/src/mathPow.java).
